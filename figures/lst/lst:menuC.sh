# run the following to generate
python tools/create-menu.py \
  -ns 3 -xi 0.1 0.3 \
  --flavour mu-e \
  --piece m2enn \
  --output-dir mu-e-nlo \
  --prog xs_main \
  --stat 0,1000,10,3000,30 \
  --stat R,10000,20,10000,40 \
  --stat V,2000,10,5000,30
