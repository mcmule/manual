# The McMule manual

*Please note that this is the **old** McMule repository that will no
longer be updated. Please head over to the new repository at
[gitlab.com/mule-tools/manual](https://gitlab.com/mule-tools/manual)*

This is the manual for the [McMule](https://gitlab.psi.ch/mcmule/mcmule) program as well as [pymule](https://gitlab.psi.ch/mcmule/pymule). If you use McMule, please consider citing
[[2007.01654]](https://arxiv.org/abs/2007.01654)

>  QED at NNLO with McMule
>
>  P. Banerjee, T. Engel, A. Signer, Y. Ulrich

You can see the manual [here](https://gitlab.psi.ch/mcmule/manual/-/jobs/artifacts/master/file/manual.pdf?job=compile) or download it [as a pdf](https://gitlab.psi.ch/mcmule/manual/-/jobs/artifacts/master/raw/manual.pdf?job=compile) or [as a ps](https://gitlab.psi.ch/mcmule/manual/-/jobs/artifacts/master/raw/manual.ps.gz?job=compile).

For further reading, please see [[1909.10244]](https://arxiv.org/abs/1909.10244),
[[2007.01654]](https://arxiv.org/abs/2007.01654) as well as

>  McMule: QED Corrections for Low-Energy Experiment
>
>  Y. Ulrich
>
>  Ph.D. thesis, University of Zurich, 2020
