%!TEX root=manual
\subsection{Preparations}

To be concrete let us assume that we want to compute two distributions, the
invariant mass of the $e\gamma$ pair, $m_{\gamma e}\equiv
\sqrt{(p_e+p_\gamma)^2}$, and the energy of the electron, $E_e$, in the
rest frame of the tau. To avoid an \ac{IR} singularity in the \ac{BR},
we have to require a minimum energy of the photon. We choose this to
be $E_\gamma \ge 10\,{\rm MeV}$ as used in~\cite{Lees:2015gea,
Oberhof:2015snl}.

As mentioned in Section~\ref{sec:structure} the quantities are defined
in the module {\tt user} ({\tt src/user.f95}). At the beginning of the
module we set
\begin{lstlisting}
  nr_q = 2
  nr_bins = 90
  min_val = (/ 0._prec, 0._prec /)
  max_val = (/ 1800._prec, 900._prec /)
\end{lstlisting}
where we have decided to have 90 bins for both distributions and {\tt
nr\_q} determines the number of distributions. The boundaries for the
distributions are set as $0 < m_{\gamma e} < 1800\,{\rm MeV}$ and $0
\le E_e \le 900\,{\rm MeV}$.

The quantities themselves are defined in the function {\tt quant} of
the module {\tt user}. This function takes arguments, {\tt q1} to {\tt
q7}. These are the momenta of the particles, arrays of length 4 with
the fourth entry the energy.  Depending on the process though not all
momenta are needed and may be zero. 


The \ac{PID}, i.e. which momentum corresponds to which particle,
can be looked up in Appendix~\ref{sec:pid} as well as the file {\tt
mat\_el.f95}.  In our case we find
\begin{lstlisting}
  use mudec, only: pm2enngav!!(q1,n,q2,q3,q4,q5, linear)
    !! mu+(p1) -> e+(p2) y(q5) + ( \nu_e \bar{\nu}_\mu )
    !! mu-(p1) -> e-(p2) y(q5) + ( \bar{\nu}_e \nu_\mu )
    !! for massive electron
    !! average over neutrino tensor taken
\end{lstlisting}
This means that we have to use {\tt q1} for the incoming $\tau$, {\tt
q2} for the outgoing $e$, and {\tt q5} for the outgoing $\gamma$.  At
\ac{NLO}, we will also need {\tt q6} for the second $\gamma$.
\begin{lstlisting}
  use mudec, only: pm2ennggav!!(p1, n1, p2, p3, p4, p5, p6)
    !! mu+(p1) -> e+(p2) \nu_e \bar{\nu}_\mu g(p5) g(p6)
    !! mu-(p1) -> e-(p2) \bar{nu}_e  \nu_\mu g(p5) g(p6)
    !! for massive (and massless) electron
    !! average over neutrino tensor taken
\end{lstlisting}
The momenta of the neutrinos do not enter, as we average over them.

Schematically, the function {\tt quant} is shown in
Listing~\ref{lst:quantlo}.  Here we have used {\tt sq} provided by
{\tt functions} to compute the square of a four-vector. We have also
specified the polarisation vector {\tt pol1} s.t. the initial tau is
considered unpolarised.  The variable {\tt pass\_cut} controls the
cuts. Initially it is set to true, to indicate that the event is kept.
Applying a cut amounts to setting {\tt pass\_cut} to false. The version
of {\tt quant} in Listing~\ref{lst:quantlo} will work for a \ac{LO}
calculation, but will need to be adapted for the presence of a second
photon in an \ac{NLO} computation. Being content with \ac{LO} for the
moment, all that remains to be done is prepare the input read by {\tt
mcmule} from {\tt stdin}, as specified in Table~\ref{tab:mcmuleinput}.

\begin{figure}
\centering
\input{figures/lst/lst:quantlo}
\renewcommand{\figurename}{Listing}
\caption{An example for the function {\tt quant} to calculate the
radiative $\tau$ decay. Note that this is only valid at \ac{LO} and
should not be used in any actual calculation.}
\label{lst:quantlo}
\end{figure}
\begin{figure}
\vspace{0.5cm}
\input{figures/tab/tab:mcmuleread}
\renewcommand{\figurename}{Table}
\caption{The options read from {\tt stdin} by \mcmule{}}
\label{tab:mcmuleinput}
\end{figure}
\begin{figure}
\input{figures/lst/lst:mcmuleread}
\renewcommand{\figurename}{Listing}
\caption{Methods to enter configuration into \mcmule{}. All four
invocations will result in the same run assuming the text file {\tt
r1.in} contains the correct data.}
\label{lst:mcmuleinput}
\end{figure}

To be concrete let us assume we want to use 10 iterations with
$1000\times 10^3$ points each for pre-conditioning and 50 iterations
with $1000\times 10^3$ points each for the actual numerical evaluation
(cf. Section~\ref{sec:stat} for some heuristics to determine the
statistics needed). We pick a random seed between 0 and $2^{31}-1$
(cf.  Section~\ref{sec:rng}), say $70\,998$, and for the input
variable {\tt which\_piece} we enter {\tt m2enng0}. This stands for
the generic process $\mu\to\nu\bar\nu e\gamma$ and 0 for tree level.
The {\tt flavour} variable is now set to {\tt tau-e} to change from
the generic process $\mu\to\nu\bar\nu e\gamma$ to the process we are
actually interested in, $\tau\to\nu\bar\nu e\gamma$.  This system is
used for other processes as well. The input variable {\tt
which\_piece} determines the generic process and the part of it that
is to be computed (i.e. tree level, real, double virtual etc.).  In a
second step, the input {\tt flavour} associates actual numbers to the
parameters entering the matrix elements and phase-space generation.

Obviously, in practice the input will typically not be given by typing
in by hand. In Listing~\ref{lst:mcmuleinput}, we have listed four
equivalent ways to input this data into {\tt mcmule}. The two
variables {\tt xinormcut1} and {\tt xinormcut2} have no effect at all
for a tree-level calculation and will be discussed below in the
context of the \ac{NLO} run. We also ignore the optional input for the
moment. 

