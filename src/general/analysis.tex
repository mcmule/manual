%!TEX root=manual
\subsection{Analysis}
\label{sec:analysis}\setcounter{enumi}{4}
Once the Monte Carlo has run, an offline analysis of the results is
required. This entails loading, averaging, and combining the data. This
is automatised in {\tt pymule} but the basic steps are
\begin{enumerate}
\setcounter{enumi}{-1}
    \item
    Load the data into a suitable analysis framework such as {\tt
    python}.

    \item
    Combine the different random seeds into one result per
    contribution and $\xc$. The $\chi^2/{\rm d.o.f.}$ of this merging
    must be small. Otherwise, try to increase the statistics or choose
    of different phase-space parametrisation.

    \item
    Add all contributions that combine into one of the physical
    contributions~\eqref{eq:nellocomb:b}. This includes any
    partitioning done in Section~\ref{sec:ps}.

    \item
    (optional) At N$^\ell$LO, perform a fit\footnote{Note that it is
    important to perform the fit after combining the phase-space
    partitionings (cf. Section~\ref{sec:ps}) but before
    adding~\eqref{eq:nellocomb:a} as this model is only valid for the
    terms of~\eqref{eq:nellocomb:b}}
    \begin{align}
        \sigma_{n+j}^{(\ell)} = c_0^{(j)} + c_1^{(j)} \log\xc +
        c_2^{(j)} \log^2\xc + \cdots + c_\ell^{(j)} \log^\ell
        = \sum_{i=0}^\ell c_i^{(j)}\log^i\xc\,.
    \label{eq:xifit}
    \end{align}
    This has the advantage that it very clearly quantifies any
    residual $\xc$ dependence. We will come back to this issue in
    Section~\ref{sec:xicut}.

    \item
    Combine all physical contributions of~\eqref{eq:nellocomb:a} into
    $\sigma^{(\ell)}(\xc)$ which has to be $\xc$ independent.

    \item
    Perform detailed checks on $\xc$ independence. This is especially
    important on the first time a particular configuration is run.
    Beyond \ac{NLO}, it is also extremely helpful to check whether
    the sum of the fits~\eqref{eq:xifit} is compatible with a
    constant, i.e.  whether for all $1\le i\le\ell$
    \begin{align}
      \Bigg|
         \frac{\sum_{j=0}^\ell        c_i^{(j)} }
              {\sum_{j=0}^\ell \delta c_i^{(j)} } \Bigg| < 1\,,
    \label{eq:xifitsum}
    \end{align}
    where $\delta c_i^{(j)}$ is the error estimate on the coefficient
    $c_i^{(j)}$.\footnote{Note that the error estimate on the sum of
    the total coefficients in \eqref{eq:xifitsum} is rather poor and
    does not include correlations between different $c_i$.} {\tt
    pymule}'s {\tt mergefkswithplot} can be helpful here.

    If \eqref{eq:xifitsum} is not satisfied or only very poorly, try
    to run the Monte Carlo again with an increased $n$.

    \item
    Merge the different estimates of~\eqref{eq:nellocomb:a} from the
    different $\xc$ into one final number $\sigma^{(\ell)}$. The
    $\chi^2/{\rm d.o.f.}$ of this merging must be small.

    \item
    Repeat the above for any distributions produced, though often
    bin-wise fitting as in Point 3 is rarely necessary or helpful.

    If a total cross section is $\xc$ independent but the
    distributions (or a cross section obtained after applying cuts)
    are not, this is a hint that the distribution (or the applied
    cuts) is not IR safe.

\end{enumerate}
These steps have been almost completely automatised in {\tt pymule}
and Mathematica. Though all steps of this pipeline could be easily
implemented in any other language by following the specification of
the file format below (Section~\ref{sec:vegasff}).
