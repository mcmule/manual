%!TEX root=manual
\begin{figure}
\input{figures/lst/lst:mollerlo}
\renewcommand{\figurename}{Listing}
\caption{An example implementation of $\M n0$ for M{\o}ller
scattering. Note that the electron mass and the centre-of-mass energy
are calculated locally. A global factor of $8e^4=128\pi^2\alpha^2$ is
included at the end.}
\label{lst:mollerlo}
\end{figure}

As an example, we will discuss how M{\o}ller scattering $e^-e^-\to
e^-e^-$ could be implemented.
\begin{enumerate}
    \item
    A new process group may need to be created if the process does not
    fit any of the presently implemented groups. This requires a new
    folder with a makefile as well as modifications to the main
    makefile as discussed in Section~\ref{sec:newpg}.

    In our case, $ee\to ee$ does not fit any of the groups, so we
    create a new group that we shall call {\tt ee}.

    \item
    Calculate the tree-level matrix elements needed at \ac{LO} and
    \ac{NLO}: $\M{n}0$ and $\M{n+1}0$. This is relatively
    straightforward and -- crucially -- unambiguous as both are finite
    in $d=4$. We will come back to an example calculation in
    Section~\ref{sec:matel}.

    \item
    A generic matrix element file is needed to store `simple' matrix
    elements as well as importing more complicated matrix elements.
    Usually, this file should not contain matrix elements that are
    longer than a few dozen or so lines. In most cases, this applies
    to $\M n0$.

    After each matrix element, the \ac{PID} needs to be denoted in a
    comment. Further, all required masses as well as the
    centre-of-mass energy, called {\tt scms} to avoid collisions with
    the function ${\tt s(pi,pj)}=2{\tt pi}\cdot{\tt pj}$, need to be
    calculated in the matrix element to be as localised as possible.

    In the case of M{\o}ller scattering, a file {\tt
    ee/ee\_mat\_el.f95} will contain $\M n0$. For example, $\M n0$ is
    implemented there as shown in Listing~\ref{lst:mollerlo}.

    \item
    Further, we need an interface file that also contains the soft
    limits. In our case this is called {\tt ee/ee.f95}.

    \item
    Because $\M{n+1}0$ is border-line large, we will assume that it
    will be stored in an extra file, {\tt ee/ee2eeg.f95}. The required
    functions are to be imported in {\tt ee/ee\_mat\_el.f95}.

    \item
    Calculate the one-loop virtual matrix element $\M n1$,
    renormalised in the \ac{OS} scheme. Of course, this could be done
    in any regularisation scheme. However, results in \mcmule{} shall
    be in the \fdh{} (or equivalently the \fdf{}) scheme. Divergent
    matrix elements in \mcmule{} are implemented as $c_{-1}$, $c_0$,
    and $c_1$
    \begin{align}
    \M n1 = \frac{(4\pi)^\epsilon}{\Gamma(1-\epsilon)}\Bigg(
        \frac{c_{-1}}\epsilon + c_0 + c_1\epsilon+\mathcal{O}(\epsilon^2)
    \Bigg)\,.
    \end{align}
    For $c_{-1}$ and $c_0$ this is equivalent to the conventions
    employed by Package-X~\cite{Patel:2015tea} up to a factor
    $1/16\pi^2$. While not strictly necessary, it is generally
    advisable to also include $c_{-1}$ in the Fortran code.

    For \ac{NLO} calculations, $c_1$ does not enter. However, we wish
    to include M{\o}ller scattering up to \ac{NNLO} and hence will
    need it sooner rather than later anyway.

    In our case, we will create a file {\tt ee/ee\_ee2eel.f95}, which
    defines a function
    \begin{lstlisting}
  FUNCTION EE2EEl(p1, p2, p3, p4, sing, lin)
    !! e-(p1) e-(p2) -> e-(p3) e-(p4)
    !! for massive electrons
  implicit none
  real(kind=prec), intent(in) :: p1(4), p2(4), p3(p4), p4(4)
  real(kind=prec) :: ee2eel
  real(kind=prec), intent(out), optional :: sing, lin
  ...
  END FUNCTION
    \end{lstlisting}
    The function shall return $c_0$ in {\tt ee2eel} and, if {\tt
    present} $c_{-1}$ and $c_1$ in {\tt sing} and {\tt lin}.

    \item
    At this stage, a new subroutine in the program {\tt test} with
    reference values for all three matrix elements should be written
    to test the Fortran implementation. This is done by generating a
    few points using an appropriate phase-space routine and comparing
    to as many digits as possible using the routine {\tt check}.

    In our case, we would construct a subroutine {\tt TESTEEMATEL} as
    shown in Listing~\ref{lst:mollertest}

    \item
    Define a default observable in {\tt user} for this process. This
    observable must be defined for any {\tt which\_piece} that might
    have been defined and test all relevant features of the
    implementation such as polarisation if applicable.

    \item
    Add the matrix elements to the integrands defined in {\tt
    integrands.f95} as discussed above. These integrands should be
    added to {\tt mcmule.f95} and a second test routine should be
    written that runs short integrations against a reference value.
    Because {\tt test\_INT} uses a fixed random seed, this is expected
    to be possible very precisely.  Unfortunately,
    COLLIER~\cite{Denner:2016kdg} might produce slightly different
    results on different machines. Hence, integrands involving
    complicated loop functions are only required to agree up to
    $\mathcal{O}(10^{-8})$.

    \item
    After some short test runs, it should be clear whether new
    phase-space routines are required. Add those, if need be, to {\tt
    phase\_space} as described in Section~\ref{sec:ps}.

    \item
    Per default the stringent soft cut, that may be required to
    stabilise the numerical integration (cf.
    Section~\ref{sec:fksfor}), is set to zero. Study what the smallest
    value is that still permits integration.

    \item
    Perform very precise $\xc$ independence studies. Tips on how to do
    this can be found in Section~\ref{sec:xicut}.

\end{enumerate}

At this stage, the \ac{NLO} calculation is complete and may, after
proper integration into \mcmule{} and adherence to coding style has
been confirmed, be added to the list of \mcmule{} processes in a new
release. Should \ac{NNLO} precision be required, the following steps
should be taken

\begin{enumerate}
\setcounter{enumi}{12}

    \item
    Calculate the real-virtual and double-real matrix elements
    $\M{n+1}1$ and $\M{n+2}0$ and add them to the test routines as
    well as integrands.

    \item
    Prepare the $n$-particle contribution $\sigma_n^{(2)}$. In a
    pinch, massified results can be used also for $\ieik(\xc)\M n1$
    though of course one should default to the fully massive results.

    \item
    Study whether the pre-defined phase-space routines are sufficient.
    Even if it was possible to use an old phase-space at \ac{NLO},
    this might no longer work at \ac{NNLO} due to the added
    complexity. Adapt and partition further if necessary, adding more
    test integrations in the process.

    \item
    Perform yet more detailed $\xc$ and soft cut analyses.

\end{enumerate}

\begin{figure}
\centering
\input{figures/lst/lst:mollertest}
\renewcommand{\figurename}{Listing}
\caption{Test routine for $ee\to ee$ matrix elements and integrands.
The reference values for the integration are yet to be determined.}
\label{lst:mollertest}
\end{figure}


In the following we comment on a few aspects of this procedure such as
the $\xc$ study (Section~\ref{sec:xicut}), the calculation of matrix
elements (Section~\ref{sec:matel}), and a brief style guide for
\mcmule{} code (Section~\ref{sec:style}).


