%!TEX root=manual
\subsection{Example calculations in Mathematica}
\label{sec:matel}
A thorough understanding of one-loop matrix elements is crucial for
any higher-order calculation. In \mcmule{}, one-loop matrix elements
either enter as the virtual contribution to \ac{NLO} corrections or
the real-virtual contribution in \ac{NNLO} calculations. In any case,
a fast numerical routine is required that computes the matrix element.

We perform all one-loop calculations in \fdf{} as this is arguably the
simplest scheme available. For theoretical background, we refer
to~\cite{Ulrich:2020phd} and references therein.

We use {\sc Qgraf} for the diagram generation. Using the in-house
Mathematica package {\tt qgraf} we convert {\sc Qgraf}'s output for
manipulation with Package-X~\cite{Patel:2015tea}. This package is
available on request through the \ac{MMCT}
\begin{lstlisting}[language=bash]
    (*@\url{https://gitlab.psi.ch/mcmule/qgraf}@*)
\end{lstlisting}

An example calculation for the one-loop calculation of
$\mu\to\nu\bar\nu e\gamma$ can be found in Listing~\ref{lst:pkgx}. Of
course this example can be made more efficient by, for example,
feeding the minimal amount of algebra to the loop integration routine.

When using {\tt qgraf} for \fdf{} some attention needs to be paid
when considering diagrams with closed fermion loops. By default, {\tt
qgraf.wl} evaluates these traces in $d$ dimensions. {\tt RunQGraf} has
an option to keep this from happening.

\begin{figure}
\input{figures/lst/lst:pkgx}
\renewcommand{\figurename}{Listing}
\caption{An example on how to calculate the renormalised one-loop
matrix element for $\mu\to\nu\bar\nu e$ in \fdf.}
\label{lst:pkgx}
\end{figure}

There is a subtlety here that only arise for complicated matrix
elements. Because the function Package-X uses for box integrals, {\tt
ScalarD0IR6}, is so complicated, no native Fortran implementation
exists in \mcmule{}.  Instead, we are defaulting to
COLLIER~\cite{Denner:2016kdg} and should directly evaluate the finite
part of the {\tt PVD} function above.  The same holds true for the
more complicated triangle functions. In fact, only the simple {\tt
DiscB} and {\tt ScalarC0IR6} are natively implemented without need for
external libraries. For any other functions, a judgement call is
necessary of whether one should {\tt LoopRefine} the finite part in
the first place. In general, if an integral can be written through
logarithms and dilogs of simple arguments (resulting in real answers)
or {\tt DiscB} and {\tt ScalarC0IR6}, it makes sense to do so.
Otherwise, it is often easier to directly link to COLLIER.

