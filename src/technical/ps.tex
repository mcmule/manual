%!TEX root=manual
\subsection{Phase-space generation}\label{sec:ps}
We use the {\tt vegas} algorithm for numerical
integration~\cite{Lepage:1980jk}.  As {\tt vegas} only works on the
hypercube, we need a routine that maps $[0,1]^{3n-4}$ to the momenta
of an $n$-particle final state, including the corresponding Jacobian.
The simplest way to do this uses iterative two-particle phase-spaces
and boosting the generated momenta all back into the frame under
consideration. An example of how this is done is shown in
Listing~\ref{lst:psn}.

As soon as we start using \ac{FKS}, we cannot use this simplistic
approach any longer. The $c$-distributions of \ac{FKS} require the
photon energies $\xi_i$ to be variables of the integration. We can fix
this by first generating the photon explicitly as
\begin{align}
\label{eq:kdef}
k_1 = p_{n+1} = \frac{\sqrt s}2\xi_1   (1,\sqrt{1-y_1  ^2}\vec e_\perp,y_1  )\,,
\end{align}
where $\vec e_\perp$ is a $(d-2)$ dimensional unit vector and the
ranges of $y_1$ (the cosine of the angle) and $\xi_1$ (the scaled
energy) are $-1\le y_1 \le 1$ and $0\le\xi_1 \le\xi_\text{max}$,
respectively. The upper bound $\xi_\text{max}$ depends on the masses
of the outgoing particles.  Following \cite{Frederix:2009yq} we
find
\begin{align}
\xi_\text{max} = 1-\frac{\Big(\sum_i m_i\Big)^2}{s}\,.
\end{align}
Finally, the remaining particles are generated iteratively again. This
can always be done and is guaranteed to work.

For processes with one or more \ac{PCS}s this approach is suboptimal.
The numerical integration can be improved by orders of magnitude by
aligning the pseudo-singular contribution to one of the variables of
the integration, as this allows {\tt vegas} to optimise the
integration procedure accordingly. As an example, consider once again
$\mu\to\nu\bar\nu e\gamma$. The \ac{PCS} comes from
\begin{align}
\M{n+1}\ell \propto \frac{1}{q\cdot k} = \frac1{\xi^2}\frac1{1-y\beta}
\,,
\end{align}
where $y$ is the angle between photon ($k$) and electron ($q$). For
large velocities $\beta$ (or equivalently small masses), this becomes
almost singular as $y\to1$. If now $y$ is a variable of the
integration this can be mediated. An example implementation is shown
in Listing~\ref{lst:psmu}.

The approach outlined above is very easy to do in the case of the muon
decay as the neutrinos can absorb any timelike four-momentum. This is
because the $\delta$ function of the phase-space was solved through
the neutrino's {\tt pair\_dec}. However, for scattering processes
where all final state leptons could be measured, this fails. Writing a
routine for $\mu$-$e$-scattering
\begin{align}
e(p_1)+\mu(p_2) \to e(p_3)+\mu(p_4) + \gamma(p_5)\,,
\end{align}
that optimises on the incoming electron is rather trivial because its
direction stays fixed s.t. the photon just needs to be generated
according to~\eqref{eq:kdef}. The outgoing electron $p_3$ is more
complicated.  Writing the $p_4$-phase-space four- instead of
three-dimensional
\begin{align}
\D\Phi_5 &= \delta^{(4)}(p_1+p_2-p_3-p_4-p_5)
 \delta(p_4^2-M^2) \Theta(E_4)
 \frac{\D^4\vec p_4}{(2\pi)^4}
 \frac{\D^3\vec p_3}{(2\pi)^32E_3}
 \frac{\D^3\vec p_5}{(2\pi)^32E_5}\,,
\end{align}
we can solve the four-dimensional $\delta$ function for $p_4$ and
proceed for the generation $p_3$ and $p_5$ almost as for the muon
decay above. Doing this we obtain for the final $\delta$ function
\begin{align}
\delta(p_4^2-M^2) =
\delta\bigg(m^2-M^2+s(1-\xi)+E_3\sqrt{s}\Big[\xi-2-y
\xi\beta_3(E_3)\Big]\bigg)\,.
\label{eq:psdel}
\end{align}
When solving this for $E_3$, we need to take care to avoid extraneous
solutions of this radical equation~\cite{Gkioulekas:2018}. We have now
obtained our phase-space parametrisation, albeit with one caveat: for
anti-collinear photons, i.e. $-1<y<0$ with energies
\begin{align}
\xi_1 = 1-\frac{m}{\sqrt{s}}+\frac{M^2}{\sqrt{s}(m-\sqrt{s}} < \xi <
\xi_\text{max}=1-\frac{(m+M)^2}{s}
\end{align}
there are still two solutions. One of these corresponds to very
low-energy electron that are almost produced at rest. This is rather
fortunate as most experiments will have an electron detection
threshold higher that this. Otherwise, phase-spaces optimised this way
also define a {\tt which\_piece} for this \term{corner region}.

There is one last subtlety when it comes to these type of phase-space
optimisations. Optimising the phase-space for emission from one leg
often has adverse effects on terms with dominant emission from another
leg. In other words, the numerical integration works best if there is
only one \ac{PCS} on which the phase-space is tuned. As most
processes have more than one \ac{PCS} we need to resort to something
that was already discussed in the original \ac{FKS}
paper~\cite{Frixione:1995ms}.  Scattering processes that involve
multiple massless particles have overlapping singular regions. The
\ac{FKS} scheme now mandates that the phase-space is partitioned in
such a way as to isolate at most one singularity per region with each
region having its own phase-space parametrisation. Similarly we have
to split the phase-space to contain at most one \ac{PCS} as well as
the soft singularity. In \mcmule{} $\mu$-$e$ scattering for instance
is split as follows\footnote{When implementing this, care must be
taken to ensure that the split is also well defined if the photon is
soft, i.e. if $\xi=0$.}
\begin{align}
1 = \theta\big( s_{15} > s_{35} \big)
  + \theta\big( s_{15} < s_{35} \big)\,,
\end{align}
with $s_{ij} = 2p_i\cdot p_j$ as usual. The integrand of the first
$\theta$ function has a final-state \ac{PCS} and hence we use the
parametrisation obtained by solving~\eqref{eq:psdel}. The second
$\theta$ function, on the other hand, has an initial-state \ac{PCS}
which can be treated by just directly parametrising the photon in the
centre-of-mass frame as per~\eqref{eq:kdef}. This automatically makes
$s_{15}\propto(1-\beta_\text{in}y_1)$ a variable of the integration.

For the double-real corrections of $\mu$-$e$ scattering, we proceed
along the same lines except now the argument of the $\delta$ function
is more complicated.





\begin{figure}
\input{figures/lst/lst:psn}
\renewcommand{\figurename}{Listing}
\caption{Example implementation of iterative phase-space. Not shown
are the checks to make sure that all particles have at least enough
energy for their mass, i.e. that $E_i\ge m_i$.}
\label{lst:psn}
\end{figure}



\begin{figure}
\input{figures/lst/lst:psmu}
\renewcommand{\figurename}{Listing}
\caption{Example implementation of a so-called \ac{FKS} phase-space
where the fifth particle is an \ac{FKS} photon that may becomes soft.
Not shown are checks whether $E_i\ge m_i$.}
\label{lst:psmu}
\end{figure}
