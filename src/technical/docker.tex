%!TEX root=manual
\subsection{Basics of containerisation}
\label{sec:docker}
\mcmule{} is Docker-compatible. Production runs should be performed
with Docker~\cite{Merkel:2014}, or its user-space complement {\sl
udocker}~\cite{Gomes:2017hct}, to facilitate reproducibility and data
retention. On Linux, Docker uses {\tt chroot} to simulate an operating
system with \mcmule{} installed. In our case, the underlying system is
Alpine Linux, a Linux distribution that is approximately 5MB in size.

\subsubsection{Terminology}
To understand Docker, we need to introduce some terms
\begin{itemize}
    \item
    An image is a representation of the system's 'hard disk'. One host
    system can have multiple images. In (u)Docker, the images can be
    listed with {\tt docker image ls} ({\tt udocker images}).

    \item
    Images can be have names, called tags, otherwise Docker assigns a
    name as the SHA256 hash.

    \item
    Because keeping multiple full file systems is rather wasteful,
    images are split into layers that can be shared among images. In
    {\sl uDocker}, these are tar files containing the changes made to
    the file system.

    \item
    To execute an image, a container needs to be generated.
    Essentially, this involves uncompressing all layers into a
    directory an {\tt chroot}ing into said directory.
\end{itemize}
It is important to note, that containers are ephemeral, i.e. changes
made to the container are not stored unless explicitly requested. This
is usually not required anyway.

For external interfacing, folders of the host system are mounted into
the container.


\subsubsection{Building images}
Docker images are built using Dockerfiles, a set of instruction on how
to create the image from external information and a base image. To
speed up building of the image, \mcmule{} uses a custom base image
called {\tt mcmule-pre} that is constructed as follows
\begin{lstlisting}[language=docker]
FROM alpine:3.11

LABEL maintainer="yannick.ulrich@psi.ch"
LABEL version="1.0"
LABEL description="The base image for the full McMule suite"

# Install a bunch of things
RUN apk add py3-numpy py3-scipy ipython py3-pip git tar gfortran gcc make curl musl-dev
RUN echo "http://dl-8.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories && \
    apk add py3-matplotlib && \
    sed -i '$ d' /etc/apk/repositories
\end{lstlisting}%stopzone
On top of this, \mcmule{} is build
\begin{lstlisting}[language=docker]
FROM yulrich/mcmule-pre:1.0.0

LABEL maintainer="yannick.ulrich@psi.ch"
LABEL version="1.0"
LABEL description="The full McMule suite"
RUN pip3 install git+gitlab.psi.ch/mcmule/pymule.git
COPY . /monte-carlo
WORKDIR /monte-carlo
RUN ./configure
RUN make
\end{lstlisting}
To build this image, run
\begin{lstlisting}[language=bash]
monte-carlo$ docker  build -t $mytagname . # Using Docker
monte-carlo$ udocker build -t=$mytagname . # Using udocker
\end{lstlisting}
The CI system uses {\sl udocker} to perform builds after each push.
Note that using {\sl udocker} for building requires a patched version
of the code that is available from the \ac{MMCT}.


\subsubsection{Creating containers and running}

In Docker, containers are usually created and run in one command
\begin{lstlisting}[language=bash]
$ docker run --rm $imagename $cmd
\end{lstlisting}%stopzone
The flag {\tt --rm} makes sure the container is deleted after it is
completed. If the command is a shell (usually {\tt ash}), the flag
{\tt -i} also needs to be provided.

For {\sl udocker}, creation and running can be done in two steps
\begin{lstlisting}[language=bash]
$ udocker create $imagename
# this prints the container id
$ udocker run $containerid $cmd
# work in container
$ udocker rm $containerid
\end{lstlisting}%stopzone
or in one step
\begin{lstlisting}[language=bash]
$ udocker run --rm $imagename $cmd
\end{lstlisting}%stopzone
Running containers can be listed with {\tt udocker ps} and {\tt docker
ps}. For further details, the reader is pointed to the manuals of
Docker and {\sl udocker}.
