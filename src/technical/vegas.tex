%!TEX root=manual
\subsection{Differential distributions and intermediary state files}
\label{sec:vegasff}

Distributions are always calculated as histograms by binning each
event according to its value for the observable $S$. This is done by
having an $(n_b\times n_q)$-dimensional array\footnote{To be precise,
the actual dimensions are $(n_b+2)\times n_q$ to accommodate under-
and overflow bins} {\tt quant} where $n_q$ is the number of histograms
to be calculated ({\tt nr\_q}) and $n_b$ is the number of bins used
({\tt nr\_bins}). The weight of each event $\D\Phi\times\mathcal{M}
\times w$ is added to the correct entry in {\tt bit\_it} where $w={\tt
wgt}$ is the event weight assigned by {\tt vegas}.

After each iteration of {\tt vegas} we add {\tt quant} (${\tt
quant}^2$) to an accumulator of the same dimensions called {\tt
quantsum} ({\tt quantsumsq}). After $i$ iterations, we can calculate
the value and error as
\begin{align}
\frac{\D\sigma}{\D S} \approx \frac{\tt quantsum}{\Delta\times i}
\qquad\text{and}\qquad
\delta\bigg(\frac{\D\sigma}{\D S}\bigg)\approx \frac1\Delta \sqrt{
    \frac{{\tt quantsumsq}-{\tt quantsum}^2/i}{i(i-1)}
}\,,
\end{align}
where $\Delta$ is the bin-size.

Related to this discussion is the concept of intermediary state
files. Their purpose is to record the complete state of the integrator
after every iteration in order to recover should the program crash --
or more likely be interrupted by a batch system. \mcmule{} uses a
custom file format {\tt .vegas} for this purpose which uses
Fortran's record-based (instead of stream- or byte-based) format. This
means that each entry starts with 32bit unsigned integer, i.e. 4 byte,
indicating the record's size and ends with the same 32bit integer. As
this is automatically done for each record, it minimises the amount of
metadata that have to be written.

The current version ({\tt v3}) must begin with the magic header and
version self-identification shown in Figure~\ref{fig:vegf:head}. The
latter includes file version information and the first five characters
the source tree's \ac{SHA1} hash, obtained using {\tt make hash}.

The header is followed by records describing the state of the
integrator as shown in Figure~\ref{fig:vegf:body}. Additionally to
information required to continue integration such as the current value
and grid information, this file also has 300 bytes for a message. This
is usually set by the routine to store information on the fate of the
integration such as whether it was so-far uninterrupted or whether
there is reason to believe it to be inconsistent. 

The latter point is particularly important. While \mcmule{} cannot
read intermediary files from a different version of the file format,
it will continue any integration for which it can read the state file.
This also includes cases where the source tree has been changed. In
this case \mcmule{} prints a warning but continues the integration
deriving potentially inconsistent results.


\begin{figure}
\begin{center}
\input{figures/tab/tab:vegf:head}
\end{center}
\caption{The magic header and version information used by {\tt v3}.
$v_1$ indicates the current version number and $v_2$ whether long
integers are used ({\tt L}) or not ({\tt N}). $s_1$-$s_5$ indicate the
first five characters of the \ac{SHA1} hash produced by the source
code at compile time ({\tt make hash}).}
\label{fig:vegf:head}

\begin{center}
\input{figures/tab/tab:vegf:body}
\end{center}
\caption{The body of a {\tt.vegas} file storing all important
information. Each horizontal line indicates as dressed record. In the
offset and length columns, all integers are in hexadecimal notation.
Negative numbers count from the end of file ({\tt EOF}).}
\label{fig:vegf:body}
\end{figure}

