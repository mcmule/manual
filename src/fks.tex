%!TEX root=manual
\section{The \texorpdfstring{FKS$^2$}{FKS2} scheme}
\label{sec:fks}

In the following we very briefly review the
FKS~\cite{Frixione:1995ms, Frederix:2009yq} and FKS$^2$
schemes~\cite{Engel:2019nfw} though this is not meant as an
introduction into these schemes. For this see~\cite{Ulrich:2019fks,
Engel:2019nfw, Ulrich:2020phd}.  Here, we just give a schematic
overview with the basic information required to understand the
structure of the code.

The core idea of this method is to render the phase-space integration
of a real matrix element finite by subtracting all possible soft
limits. The subtracted pieces are partially integrated over the phase
space and combined with the virtual matrix elements to form finite
integrands.

The NLO corrections $\sigma^{(1)}$ to a cross section are split into a
$n$ particle and $(n+1)$ particle contribution and are written as
\begin{subequations}
\label{eq:nlo:4d}
\begin{align}
%\begin{split}
\sigma^{(1)} &=
\sigma^{(1)}_n(\xc) + \sigma^{(1)}_{n+1}(\xc) \, , \label{eq:nlo}\\
\sigma^{(1)}_n(\xc) &= \int
\ \D\Phi_n^{d=4}\,\Bigg(
    \M n1
   +\ieik(\xc)\,\M n0
\Bigg) = 
\int \ \D\Phi_n^{d=4}\, \fM n1
\,,
%\end{split}
\label{eq:nlo:n}
\\
\sigma^{(1)}_{n+1}(\xc) &= \int 
\ \D\Phi^{d=4}_{n+1}
  \cdis{\xi_1} \big(\xi_1\, \fM{n+1}0 \big)
\label{eq:nlo:n1}
\, .
\end{align}
\end{subequations}
In \eqref{eq:nlo:n1}, $\xi_1$ is a variable of the $(n+1)$ parton
phase space $\D\Phi^{d=4}_{n+1}$ that corresponds to the (scaled)
energy of the emitted photon. For $\xi_1\to 0$ the real matrix element
$\fM{n+1}0$ develops a singularity. The superscripts $(0)$ and $f$
indicate that the matrix element is computed at tree level and is
finite, i.e. free of expicit infrared poles $1/\epsilon$. In order to
avoid an implicit infrared pole upon integration, the $\xi_1$
integration is modified by the factor $\xi_1 (1/\xi_1)_c$, where the
distribution $(1/\xi_1)_c$ acts on a test function $f$ as
\begin{align}
  \label{eq:xidist}
\int_0^1\D\xi_1\, \cdis{\xi_1}\, f(\xi_1)
&\equiv
\int_0^1\D\xi_1\,\frac{f(\xi_1)-f(0)\theta(\xc-\xi_1)}{\xi_1}
\,.
\end{align}
Thus, for $\xi_1 < \xc$, the integrand is modified through the
subtraction of the soft limit. This renders the integration
finite. However, it also modifies the result. The missing piece of the
real corrections can be trivially integrated over $\xi_1$. This
results in the integrated eikonal factor $\ieik(\xc)$ times the
tree-level matrix element for the $n$ particle process, $\M n0$. The
factor $\ieik(\xc)$ has an explicit $1/\epsilon$ pole that cancels
precisely the corresponding pole in the virtual matrix element
$\M n1$. Thus, the combined integrand of \eqref{eq:nlo:n} is free of
explicit poles, hence denoted by $\fM n1$, and can be integrated
numerically over the $n$ particle phase space $\D\Phi_n^{d=4}$. 

The parameter $\xc$ that has been introduced to split the real
corrections can be chosen arbitrarily as long as
\begin{align}
  \label{eq:xic}
0<\xc\le\xi_\text{max} = 1-\frac{\big(\sum_i m_i\big)^2}{s}\,,
\end{align}
where the sum is over all masses in the final state. The $\xc$
dependence has to cancel exactly between \eqref{eq:nlo:n} and
\eqref{eq:nlo:n1} since at no point any approximation was made in the
integration. Checking this independence is a very useful tool to test
the implementation of the method, as well as its numerical stability.

The finite matrix element $\fM n1$ is simply the first-order expansion
of the general YFS exponentiation formula for soft singularities
\begin{align}
e^{\ieik}\, \sum_{\ell = 0}^\infty \M{n}{\ell} = 
\sum_{\ell = 0}^\infty \fM{n}{\ell}
=  \M n0 +
  \Big(\M n1 +\ieik(\xc)\,\M n0\Big) + \mathcal{O}(\alpha^2)\,,
\label{eq:yfsnew}
\end{align}
where we exploited the implicit factor $\alpha$ in $\ieik$.

For QED with massive fermions this scheme can be extended to NNLO and,
in fact beyond. The NNLO corrections are split into three parts
\begin{subequations}
\label{eq:nnlo:4d}
\begin{align}
\begin{split}
\sigma^{(2)}_n(\xc) &= \int
\ \D\Phi_n^{d=4}\,\bigg(
    \M n2
   +\ieik(\xc)\,\M n1
   +\frac1{2!}\M n0 \ieik(\xc)^2
\bigg) = 
\int \ \D\Phi_n^{d=4}\, \fM n2
\,,
\end{split}\label{eq:nnlo:n}
\\
\sigma^{(2)}_{n+1}(\xc) &= \int 
\ \D\Phi^{d=4}_{n+1}
  \cdis{\xi_1} \Big(\xi_1\, \fM{n+1}1(\xc)\Big)
\,,\\
\sigma^{(2)}_{n+2}(\xc) &= \int
\ \D\Phi_{n+2}^{d=4}
   \cdis{\xi_1}\,
   \cdis{\xi_2}\,
     \Big(\xi_1\xi_2\, \fM{n+2}0\Big) \, .
\end{align}
\end{subequations}
Thus we have to evaluate $n$ parton contributions, single-subtracted
$(n+1)$ parton contributions, and double-subtracted $(n+2)$ parton
contributions. This structure will be mirrored in the Fortran
code. The $\xc$ dependence cancels, once all three contributions are
taken into account. For this subtraction method we need the matrix
elements with massive fermions. If the two-loop amplitudes are
available only for massless fermions, it is possible to use
massification~\cite{Engel:2018fsb}.


%%%%%%%%%%%%%%%%%%%%
\subsection{\texorpdfstring{FKS$^\ell$: extension to N$^\ell$LO}{FKSl: extension to NlLO}}
\label{sec:nllo}
%\subsection{${\rm N}^\ell{\rm LO}$ generalisation}
%%%%%%%%%%%%%%%%%%%%

The pattern that has emerged in the previous cases leads to the 
following extension to an arbitrary order $\ell$ in perturbation
theory:
\begin{subequations}
\begin{align}
\D\sigma^{(\ell)} &= \sum_{j=0}^\ell \bbit{\ell}{n+j}(\xc)\, ,
\label{eq:nellocomb:a}
\\
\bbit{\ell}{n+j}(\xc) &=  \D\Phi_{n+j}^{d=4}\,\frac{1}{j!} \, 
\bigg( \prod_{i=1}^j \cdis{\xi_i} \xi_i \bigg)\,
 \fM{n+j}{\ell-j}(\xc)\,.
\label{eq:nellocomb:b}
\end{align}
\label{eq:nellocomb}%
\end{subequations}
The eikonal subtracted matrix elements
\begin{align}
\fM{m}\ell &= \sum_{j=0}^\ell\frac{\ieik^j}{j!} \M{m}{\ell-j}\,,
\end{align}
(with the special case $\fM{m}0 = \M{m}0$ included) are free from
$1/\epsilon$ poles, as indicated in \eqref{eq:yfsnew}. Furthermore, the
phase-space integrations are manifestly finite. 



