%!TEX root=manual
\section{Structure of \mcmule{}}
\label{sec:structure}

\mcmule{} is written in Fortran 95 with helper and analysis tools
written in {\tt python}\footnote{Additionally to the {\tt python} tool
a Mathematica tool is available.}.  The code is written with two kinds
of applications in mind. First, several processes are implemented,
some at NLO, some at NNLO. For these, the user can define an arbitrary
(infrared safe), fully differential observable and compute cross
sections and distributions. Second, the program is set up such that
additional processes can be implemented by supplying the relevant
matrix elements.

To obtain a copy of \mcmule{} we recommend the following approach
\begin{lstlisting}[language=bash]
$ git clone --recursive https://gitlab.psi.ch/mcmule/mcmule
\end{lstlisting}%stopzone
\noindent To build \mcmule{}, a Fortran compiler such as {\tt
gfortran} and a python installation is needed. The main executable can
be compiled by running
\begin{lstlisting}[language=bash]
$ ./configure
$ make mcmule
\end{lstlisting}
Alternatively, we provide a Docker container~\cite{Merkel:2014} for
easy deployment and legacy results (cf. Section~\ref{sec:docker}). In
multi-user environments, {\sl udocker}~\cite{Gomes:2017hct} can be
used instead. In either case, a pre-compiled copy of the code can be
obtained by calling
\begin{lstlisting}[language=bash]
$ docker pull yulrich/mcmule  # requires Docker to be installed
$ udocker pull yulrich/mcmule # requires uDocker to be installed
\end{lstlisting}
We provide instructions on how \mcmule{} is used in
Section~\ref{sec:example}.


When started, {\tt mcmule} reads options from {\tt stdin} as specified
in Table~\ref{tab:mcmuleinput} (cf. Section~\ref{sec:example}). The
value and error estimate of the integration is printed to {\tt stdout}
and the full status of the integration is written in a
machine-readable format into a folder called {\tt out/} (see below).

\mcmule{} consists of several modules with a simple, mostly hierarchic
structure. The relation between the most important Fortran modules is
depicted in Figure~\ref{fig:structure}. A solid arrow indicates
``using'' the full module, whereas a dashed arrow is indicative of
partial use. In what follows we give a brief description of the
various modules and mention some variables that play a prominent role
in the interplay between the modules.


\begin{figure}
  \centering
  \input{figures/tikz/fig:structure.tex}
  \caption{The structure of \mcmule{}.}
  \label{fig:structure}
\end{figure}



\begin{description}
    \item[{\tt global\_def}:]
    This module simply provides some parameters such as fermion masses
    that are needed throughout the code. It also defines {\tt prec} as
    a generic type for the precision used.\footnote{For quad precision
    {\tt prec=16} and the compiler flag {\tt -fdefault-real-16} is
    required.} Currently, this simply corresponds to double precision.

    \item[{\tt functions}:]
    This module is a library of basic functions that are needed at
    various points in the code. This includes dot products, eikonal
    factors, the integrated eikonal, and an interface for scalar
    integral functions among others.

    \item[{\tt collier}:]
    This is an external module~\cite{Denner:2016kdg, Denner:2010tr,
    Denner:2005nn, Denner:2002ii}. It will be linked to \mcmule{}
    during compilation and provides the numerical evaluations of the
    scalar and in some cases tensor integral functions in {\tt
    functions}.

    \item[{\tt phase\_space}:]
    The routines for generating phase-space points and their weights
    are collected in this module. Phase-space routines ending with
    {\tt FKS} are prepared for the \ac{FKS} subtraction procedure
    with a single unresolved photon. In the weight of such routines a
    factor $\xi_1$ is omitted to allow the implementation of the
    distributions in the \ac{FKS} method. This corresponds to a global
    variable {\tt xiout1}. This factor has to be included in the
    integrand of the module {\tt{integrands}}. Also the variable {\tt
    ksoft1} is provided that corresponds to the photon momentum
    without the (vanishing) energy factor $\xi_1$. Routines ending
    with {\tt FKSS} are routines with two unresolved photons.
    Correspondingly, a factor $\xi_1\,\xi_2$ is missing in the weight
    and {\tt xiout1} and {\tt xiout2}, as well as {\tt ksoft1} and
    {\tt ksoft2} are provided. To ensure numerical stability it is
    often required to tune the phase-space routine to a particular
    kinematic situation.

    \item[{\tt \{pg\}\_mat\_el}]:
    Matrix elements are grouped into \term{process groups} such as
    muon decay ({\tt mudec}) or $\mu$-$e$ and $\mu$-$p$ scattering
    ({\tt mue}). Each process group contains a {\tt mat\_el} module
    that provides all matrix elements for its group.  Simple matrix
    elements are coded directly in this module. More complicated
    results are imported from sub-modules not shown in
    Figure~\ref{fig:structure}. A matrix element starting with {\tt P}
    contains a polarised initial state.  A matrix element ending in
    {\tt av} is averaged over a neutrino pair in the final state.

    \item[{\tt \{pg\}}:]
    In this module the soft limits of all applicable matrix elements
    of a process group are provided to allow for the soft subtractions
    required in the \ac{FKS} scheme. These limits are simply the
    eikonal factor evaluated with {\tt ksoft} from {\tt phase\_space}
    times the reduced matrix element, provided through {\tt mat\_el}.

    This module also functions as the interface of the process group,
    exposing all necessary functions that are imported by

    \item[{\tt mat\_el},] which collects all matrix elements as well
    as their particle labelling or \aterm{particle
    identification}{PID}.

    \item[{\tt user}:]
    For a user of the code who wants to run for an already implemented
    process, this is the only relevant module.  At the beginning of
    the module, the user has to specify the number of quantities to be
    computed, {\tt nr\_q}, the number of bins in the histogram, {\tt
    nr\_bins}, as well as their lower and upper boundaries, {\tt
    min\_val} and {\tt max\_val}. The last three quantities are arrays
    of length {\tt nr\_q}. The quantities themselves, i.e. the
    measurement function, is to be defined by the user in terms of the
    momenta of the particles in {\tt quant}. Cuts can be applied by
    setting the logical variable {\tt pass\_cut} to
    false\footnote{Technically, {\tt pass\_cut} is a list of length
    {\tt nr\_q}, allowing to decide whether to cut for each histogram
    separately.}. Some auxiliary functions like (pseudo)rapidity,
    transverse momentum etc. are predefined in {\tt functions}. Each
    quantity has to be given a name through the array {\tt names}.

    Further, {\tt user} contains a subroutine called {\tt inituser}.
    This allows the user to read additional input at runtime, for
    example which of multiple cuts should be calculated. It also
    allows the user to print some information on the configuration
    implemented. Needless to say that it is good idea to do this for
    documentation purposes.

    \item[{\tt vegas}:]
    As the name suggests this module contains the adaptive Monte Carlo
    routine {\tt vegas}~\cite{Lepage:1980jk}.  The binning routine
    {\tt bin\_it} is also in this module, hence the need for the
    binning metadata, i.e. the number of bins and histograms ({\tt
    nr\_bins} and {\tt nr\_q}, respectively) as well as their bounds
    ({\tt min\_val} and {\tt max\_val}) and names, from {\tt user}.

    \item[{\tt integrands}:]
    In this module the functions that are to be integrated by {\tt
    vegas} are coded. There are three types of integrands:
    non-subtracted, single-subtracted, and double-subtracted
    integrands, corresponding to the three parts of the ${\rm FKS}^2$
    scheme~\cite{Engel:2019nfw,Ulrich:2020phd}.  The matrix elements
    to be evaluated and the phase-space routines used are set using
    function pointers through a subroutine {\tt initpiece}.  The
    factors $\xi_i$ that were omitted in the phase-space weight have
    to be included here for the single- and double-subtracted
    integrands.

    \item[{\tt mcmule}:]
    This is the main program, but actually does little else than read
    the inputs and call {\tt vegas} with a function provided by {\tt
    integrands}.

    \item[{\tt test}:]
    For developing purposes, a separate main program exists that is
    used to validate the code after each change. Reference values for
    matrix elements and results of short integrations are stored here
    and compared against.

\end{description}

The library of matrix elements deserves a few comments. As matrix
elements quickly become very large, we store them separately from the
main code. This makes it also easy to extend the program by minimising
the code that needs to be changed.

We group matrix elements into process groups, \term{generic
processes}, and \term{generic pieces} as indicated in
Appendix~\ref{sec:pieces}. The generic process is a prototype for the
physical process such as $\ell p\to\ell p$  where the flavour of the
lepton $\ell$ is left open. The generic piece describes a part of the
calculation such as the real or virtual corrections, i.e. the
different pieces of~\eqref{eq:nlo:4d} (or
correspondingly~\eqref{eq:nnlo:4d} at NNLO), that themselves may be
further subdivided as is convenient.  In particular, in some cases a
generic piece is split into various partitions (cf.
Section~\ref{sec:ps} for details on why that is important).

When running {\tt mcmule}, the code generates a statefile from which
the full state of the integrator can be reconstructed should the
integration be interrupted (cf. Section~\ref{sec:vegasff} for
details). This makes the statefile ideal to also store results in a
compact format.  To analyse these results, we provide a python tool
{\tt pymule}, additionally to the main code for \mcmule{}. {\tt
pymule} uses {\tt numpy}~\cite{Walt:2011np} for data storage and {\tt
matplotlib} for plotting~\cite{Hunter:2007mp}. While {\tt pymule} works with
any python interpreter, {\tt IPython}~\cite{Perez:2007ip} is
recommended. We will encounter {\tt pymule} in
Section~\ref{sec:analyse} when we discuss how to use it to analyse
results. A full list of functions provided can be found in
Appendix~\ref{sec:pymule}.




